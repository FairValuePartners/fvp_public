%function ReadBondsEikonApi ()
%% Initialization
paths.eikonApiScripts='I:\Fernando\EikonMatlabTimeSeriesExample\';
paths.eikonSdk='I:\Fernando\eikonSdk\';%path to the required .dll files, see timeSeriesExample.m in Fernando/EikonMatlabTimeSeriesExample
addpath(paths.eikonApiScripts);
useGetFile=0;
paths.orgPath=cd;
if useGetFile
    
else
    paths.outstanding='I:\Fernando\';
    paths.notOutstanding='I:\Fernando\';
    fileNameOutstanding='Search_1508879404960.xlsx';
    fileNameNotOutstanding='Search_1508879537227.xlsx';
end

%Read Csv of RICs to read
cd(paths.outstanding)
[~,~,rawOutstanding] = xlsread(fileNameOutstanding);
cd(paths.notOutstanding)
[~,~,rawNotOutstanding] = xlsread(fileNameNotOutstanding);
cd(paths.orgPath)
outstandingRics=rawOutstanding(2:end,7);
notOutstandingRics=rawNotOutstanding(2:end,7);

% fileId=fopen(fileNameOutstanding);
% values=textscan(fileId,'%s','Delimiter',','); %read the file
% values=values{1,1};
% fclose(fileId);
% fileId=fopen(fileNameOutstanding);
% %get number of columns:
% tline=fgetl(fileId);
% firstRow=textscan(tline,'%s','Delimiter',',');
% numOfColumns=length(firstRow{1});
% fclose(fileId);
% values=reshape(values,numOfColumns,[])'; %change format
historicalData=struct();
assignin('base','historicalData',historicalData);
outstandingRics={'912828TW0=','912828XV7='};
for iRic=1 : length(outstandingRics)
    global t %example from eikon uses global
    %t=treikonnet('I:\Fernando\eikonSdk\');%path to the required .dll files, see timeSeriesExample.m in Fernando/EikonMatlabTimeSeriesExample
    t=treikonnet(paths.eikonSdk);
    
    t.Services.Initialize('MyMatlabEikonTestApp'); %method input is application ID string
    timeSeries = t.Services.TimeSeries;
    if strcmp(t.Services.State,'Up')
        %     historicalData=struct();
        %     assignin('base','historicalData',historicalData);
        %     outstandingRics={'912828TW0=','912828XV7='};
        %     outstandingRics={'912828XV7='};
        %    outstandingRics={'912828TW0='};
        
        %     for iRic=1 : length(outstandingRics)
        currRic=outstandingRics{iRic};
        
        historicalData=evalin('base','historicalData');
        
        %only use the following when the for loop above is disabled:
        %currRic='912828XS4='; %row 85
        %currRic='912828XV7='; %row 89
        %currRic='912828TW0='; %works
        %currRic='302051AK3=';
        %currRic='302051AL1=';
        
        currRicField=currRic;
        currRicField(5:end+4)=currRicField(1:end);
        currRicField(1:4)='RIC_';
        currRicField=regexprep(currRicField,'=','_');
        %historicalData.(currRicField)={};
        historicalData.(currRicField)={'Timestamp','Open','High','Low','Close'};
        assignin('base','historicalData',historicalData);
        assignin('base','currRicField',currRicField);
        assignin('base','currRic',currRic);
        assignin('base','iRic',iRic);
        SendTimeSeriesRequest(timeSeries,currRic);
        %end
    else
        addlistener(timeSeries,'ServiceInformationChanged',@OnTimeSeriesServiceInformationChanged);
        disp('Server returned a ''down'' state, please make sure that Eikon is running in this computer')
    end
end

    function r = OnEikonStateChanged(~,eventArgs)
        disp('Eikon StateChanged event called');
        disp(System.String.Concat('Eikon state is ',eventArgs.State));
        r = char(eventArgs.State);
    end

    function r = OnTimeSeriesServiceInformationChanged(timeSeries,eventArgs)
        disp('TimeSeries ServiceInformationChanged event called');
        disp(System.String.Concat('Timeseries service state is ',eventArgs.Information.State));
        r = char(eventArgs.Information.State);
        if strcmp(r,'Up')
            SendTimeSeriesRequest(timeSeries);
        end
    end

    function SendTimeSeriesRequest(timeSeries,currRic)
        %disp('Sending timeseries request');
        
        timeSeriesRequestSetup = timeSeries.SetupDataRequest(currRic);
        timeSeriesRequestSetup.WithView('BID');
        %timeSeriesRequestSetup.WithView('NDA_RAW');
        %added FY
        %timeSeriesRequestSetup.WithInterval(ThomsonReuters.Desktop.SDK.DataAccess.TimeSeries.CommonInterval.Daily);
        %k=timeSeries.GetViewList(currRic,@viewsCallback,@viewsError);
        %timeSeries
        %end added FY
        timeSeriesRequestSetup.WithNumberOfPoints(365*30); %30 years just to get the maximum possible
        timeSeriesRequestSetup.OnDataReceived(@DataReceivedCallback);
        %evalin('base','currRic')
        %         historicalData=evalin('base','historicalData');
        %         currRicField=currRic;
        %         currRicField(5:end+4)=currRicField(1:end);
        %         currRicField(1:4)='RIC_';
        %         currRicField=regexprep(currRicField,'=','_');
        %         historicalData.(currRicField)={};
        timeSeriesRequestSetup.CreateAndSend();
        %disp('Order sent')
        r=1;
    end

    function r = DataReceivedCallback(chunk)
        % The data is returned in chunks. IsLast property of the chunk
        % object indicates if data retrieval is complete or if more data is
        % expected to be retrieved.
        %         disp(System.String.Concat('RIC: ',chunk.Ric));
        %         disp(System.String.Concat('Is this the last chunk: ',chunk.IsLast));
        historicalData=evalin('base','historicalData');
        %historicalData
        
        
        %new approach:
        %         currRic=chunk.Ric;
        %         currRicField=currRic;
        %         currRicField(5:end+4)=currRicField(1:end);
        %         currRicField(1:4)='RIC_';
        %         currRicField=regexprep(currRicField,'=','_');
        %         currRic
        %disabled previous approach:
        currRicField=evalin('base','currRicField');
        currRic=evalin('base','currRic');
        
        
        evalin('base','iRic')
        %chunk.Ric
        records = chunk.Records.GetEnumerator();
        timeSeriesDataOutput = {};
        k=1;
        loopFlag=false;
        %timeSeriesDataOutput(1,1:5)={'Timestamp','Open','High','Low','Close'};
        while records.MoveNext
            bar = records.Current.ToBarRecord;
            ts = char(records.Current.Timestamp.ToString);
            timeSeriesDataOutput(k,1:5) = {ts,bar.Open.Value,...
                bar.High.Value,bar.Low.Value,bar.Close.Value};
            k=k+1;
            loopFlag=true;
        end
        %
        %         if loopFlag
        %             historicalData.(currRicField)={'Timestamp','Open','High','Low','Close'};
        %
        %             disp('1')
        %         else
        %             %Append at the bottom
        %             historicalData.(currRicField)=[historicalData.(currRicField);timeSeriesDataOutput];
        %
        %             disp('2')
        %         end
        historicalData.(currRicField)=[historicalData.(currRicField);timeSeriesDataOutput];
        %Assign to base:
        assignin('base','historicalData',historicalData);
        %         assignin('base','currRic',currRic);
        %         assignin('base','currRicField',currRicField);
        %         disp(currRicField)
        %         historicalData
        %pause(eps)
        
        
        
        %disp(timeSeriesDataOutput);
        
        %disp('chunk');
    end

    function viewList=viewsCallback(viewsResponse,k)
        %rec=viewsResponse.GetEnumerator();
        
        disp('sadf')
    end

    function errorView=viewsError(viewsResponseError)
        disp('asdf')
        
    end

    function ProcessViews (input1, iput2)
        disp('asdf')
    end
%% Finalization
% rmpath(paths.eikonApiScripts);
% 
% end